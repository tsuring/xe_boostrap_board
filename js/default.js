jQuery(document).ready(function($) {
    /* 사용자 변수 폼 클래스 추가 시작 */
    $('.exForm').find('input[type=text]').addClass('form-control');
    $('.exForm').find('textarea').addClass('form-control');
    /* 사용자 변수 폼 클래스 추가 끝 */
    /* 검색 종류 선택 시작 */
    $('.search_option li').click(function (e) {
        e.preventDefault();
        var selected = $(this).attr('data-target');
        var selectedtext = $(this).text();
        $('#search_target').val(selected);
        $('.search_target').html(selectedtext);
    });
    /* 검색 종류 선택 끝 */
    /* 분류 선택 시작 */
    $('.category_select li').click(function (e) {
        e.preventDefault();
        var selected = $(this).attr('category_srl');
        var selectedtext = $(this).text();
        $('#category_srl').val(selected);
        $('.category_target').html(selectedtext);
    });
    var selected_cate = $("li[selected='selected']").attr('category_srl');
    var selected_cate_name = $("li[selected='selected']").text();
    if (selected_cate != undefined) {
        $('#category_srl').val(selected_cate);
        $('.category_target').html(selected_cate_name);
    }
    /* 분류 선택 끝 */

    /* 색깔 선택 시작 */
    $('.jPicker .Icon').live('mousedown',function(){
        $('#title_color').prev().css('visibility','hidden')
    });
    $('.jPicker.Container .Button input').live('mousedown',function(){
        $('#title_color').prev().click();
    });
    $('.board_write button[type=submit]').click(function(){
        var t = $('#title_color');
        if(!t.length) return;
        t.val(t.val().replace('#',''));
        if(t.val()=='transparent') t.val('');
    });
    /* 색깔 선택 끝 */
    $(".get_sign div").mCustomScrollbar({});

    /* 댓글 html 사용 시작 */
    var cmtWrt = $(".xe_bootstrap_board").find('form.write_comment textarea');
    cmtWrt.each(function() {
        $.getScript("modules/editor/tpl/js/editor_common.min.js",function() {
            editorStartTextarea(cmtWrt.attr('id').split('_')[1], 'content', 'comment_srl');
        });
    });
    /* 댓글 html 사용 끝 */
});

function reComment(doc_srl,cmt_srl,edit_url){
    var o = jQuery('#re_cmt').eq(0);
    o.find('form').attr('editor_sequence', cmt_srl);
    o.find('input[name=error_return_url]').val('/'+doc_srl);
    o.find('input[name=mid]').val(current_mid);
    o.find('input[name=document_srl]').val(doc_srl);
    o.find("input[id^=htm_]").attr('id', 'htm_' + cmt_srl);
    o.find('textarea').attr('id', 'editor_' + cmt_srl);
    o.clone().appendTo(jQuery('#comment_'+cmt_srl)).fadeIn().find('input[name=parent_srl]').val(cmt_srl);
    o.find('textarea').focus();
    editorStartTextarea(cmt_srl, 'content', 'comment_srl');
}

var oldcomment = {};

function modifyComment(doc_srl,cmt_srl,content){
    oldcomment[cmt_srl] = jQuery('#comment_'+cmt_srl).html();
    jQuery('#comment_'+cmt_srl).html(jQuery('#write_comment').html());
    jQuery('#comment_'+cmt_srl).css('padding', 0);
    jQuery('#comment_'+cmt_srl).find('input[name=error_return_url]').val('/'+doc_srl);
    jQuery('#comment_'+cmt_srl).find('form').attr('editor_sequence', cmt_srl);
    jQuery('#comment_'+cmt_srl).find('form').append("<input type='hidden' name='parent_srl' value=''/>");
    jQuery('#comment_'+cmt_srl).find('form').append('<input type="hidden" name="_filter" value="insert_comment">');
    jQuery('#comment_'+cmt_srl).find('input[name=document_srl]').val(doc_srl);
    jQuery('#comment_'+cmt_srl).find('input[name=comment_srl]').val(cmt_srl);
    jQuery('#comment_'+cmt_srl).find('input[name=content]').val(content);
    jQuery('#comment_'+cmt_srl).find("input[id^=htm_]").attr('id', 'htm_' + cmt_srl);
    jQuery('#comment_'+cmt_srl).find('textarea').attr('id', 'editor_' + cmt_srl);
    jQuery('#comment_'+cmt_srl).find('textarea').focus();
    jQuery('#comment_'+cmt_srl).find('.btnArea').append("<button onclick='modifycancelComment("+ cmt_srl+ "); return false;' class='btn btn-default'>취소</button>");
    jQuery.getScript("modules/editor/tpl/js/editor_common.min.js",function() {
        editorStartTextarea(jQuery('#comment_' + cmt_srl).find('form.write_comment textarea').attr('id').split('_')[1], 'content', 'comment_srl');
    });
}

function modifycancelComment(cmt_srl) {
    if (oldcomment == '') {
        alert('잘못된 요청 입니다.');
        return;
    } else {
        jQuery('#comment_' + cmt_srl).html(oldcomment[cmt_srl]);
        jQuery('#comment_'+cmt_srl).css('padding', '');
    }
}

function deleteComment(doc_srl, cmt_srl) {
    if (!doc_srl && !cmt_srl) {
        alert('잘못된 요청 입니다.');
        return;
    }
    if (confirm("정말 삭제하시겠습니까??") == true) {
        var result = {}
        jQuery.exec_json("board.procBoardDeleteComment", {"document_srl":doc_srl, "comment_srl":cmt_srl}, complateDeletecomment);
    }else{   //취소
        return;
    }
}

function complateDeletecomment(res_obj) {
    alert(res_obj.message);
    if(res_obj.error == 0) {
        jQuery('.feedback',parent.document).load('/' + res_obj.document_srl + ' .feedback');
    }
}

function commentPagenav(doc_srl, cmt_page) {
    jQuery('.feedback',parent.document).load('/index.php?document_srl=' + doc_srl + '&cpage=' + cmt_page + ' .feedback', function() {
        jQuery('html, body').animate({ scrollTop : jQuery('.feedback').offset().top }, 350);
    });
}